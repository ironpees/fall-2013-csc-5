#include <iostream>

using namespace std;

int main()
{
    int singles, doubles, triples, home_runs, at_bat, slugging_percentage;
    
    cout << "Enter your single hits.\n";
    cin >> singles;
    cout << "Enter your double hits.\n";
    cin >> doubles;
    cout << "Enter your triples hits.\n";
    cin >> triples;
    cout << "Enter your home runs.\n";
    cin >> home_runs;
    cout << "Enter your times at bat.\n";
    cin >> at_bat;
    
    slugging_percentage = ((singles)+(2*doubles)+(3*triples)+(4*home_runs)) /
            at_bat;
    
    cout << "Your Slugging percentage is " << slugging_percentage << "." <<endl;
    
    return 0;
}


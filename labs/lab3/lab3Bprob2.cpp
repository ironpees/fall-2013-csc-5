#include <iostream>
using namespace std;
int main()
{
  int grade;
  cout << "Enter a score between 1 and 100.\n";
  cin >> grade;
  
  if (grade > 89)
  {
    cout << "You got an A\n";
  }
  
  if (grade > 79 && grade < 90)
  {
    cout << "You got a B\n";
  }
  
  if (grade > 69 && grade <80)
  {
    cout << "You got a C\n";
  }
  
  if (grade > 59 && grade < 70)
  {
    cout << "You got a D\n";
  }
  if (grade < 60) 
  {
    cout << "Study more, you got an F\n";
  }
  
  
  
  return 0;

}
#include <iostream>
using namespace std;

int main()
{
    double grade;
    
    cout << "Enter your score\n";
    cin >> grade;
    
    
    if(grade > 100)
    {
        cout << "A+" << endl;
    }
    if(grade >= 93 && grade <= 100)
    {
        cout << "A" << endl;
    }
    if(grade < 93 && grade >=90)
    {
        cout << "A-" << endl;
    }
    if(grade >= 87 && grade < 90)
    {
        cout << "B+" << endl;
    }
    if(grade >=83 && grade < 87)
    {
        cout << "B" << endl;
    }
    if(grade < 83 && grade >= 80)
    {
        cout << "B-" << endl;
    }
    if(grade < 80)
    { 
        cout << "Study more" << endl;
    }
    
    
    return 0;
    
}

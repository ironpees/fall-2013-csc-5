#include <iostream>
#include <iomanip>

using namespace std;
int main()
{
    int x,y;
    
    cout << "Enter 2 integers under 1000\n";
    cin >> x;
    cin >> y;

    
    cout << left << setfill(' ') << "X = " << setw(3) << x << " " << "Y = " << setw(3) << y << endl;
    cout << setfill('-') << setw(80)<< "" << endl;
    cout << setfill('-') << setw(30)<< "" << endl;
    cout << setfill(' ') << "X = " << setw(3) << y << " " << "Y = " << setw(3) << x << endl;

    return 0;
}
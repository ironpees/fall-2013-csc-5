#include <iostream>
using namespace std;

int main()
{
    int first_test, second_test, third_test, average_grade;
    
    cout << "Enter your first test score\n";
    cin >> first_test;
    cout << "Enter second test score\n";
    cin >> second_test;
    cout << "Enter third test score\n";
    cin >> third_test;
    
    average_grade = first_test, second_test, third_test;
    
    cout << "Your average test score is " << average_grade << endl;
    
    return 0;
}
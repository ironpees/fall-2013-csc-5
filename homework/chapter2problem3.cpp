#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int john_1, john_2, john_3, john_4, mary_1, mary_2, mary_3,
            mary_4, matthew_1, matthew_2, matthew_3, matthew_4;
    
    double quiz_1_average, quiz_2_average, quiz_3_average, quiz_4_average;
    
    cout << "You are going to be entering your test scores\n";
    
    cout << "John, you are up first\n";
    cout << "Please enter your first score john\n";
    cin >> john_1;
    cout << "Please enter your second score john\n";
    cin >> john_2;
    cout << "Please enter your third score john\n";
    cin >> john_3;
    cout << "What was your fourth score\n";
    cin >> john_4;
    
    cout << "Mary, its your turn\n";
    cout << "Please enter your first score\n";
    cin >> mary_1;
    cout << "please enter your second score mary\n";
    cin >> mary_2;
    cout << "Please enter your third score\n";
    cin >> mary_3;
    cout << "What was your last score\n";
    cin >> mary_4;
    
    cout << "Your turn Matthew\n";
    cout << "Please enter your first test score matthew\n";
    cin >> matthew_1;
    cout << "What is your second score\n";
    cin >> matthew_2;
    cout << "What was the third score\n";
    cin >> matthew_3;
    cout << "what was your last score\n";
    cin >> matthew_4;
    cout << endl;
    cout << endl;
    
    
    cout << setw(10) << left << setw(8) << "name" << setw(8) <<
             "Quiz 1" << setw(8) << "Quiz 2" << setw(8) << "Quiz 3" << setw(8)
            << "Quiz 4"
            << endl;
    cout << setw(8) << left << "----" << setw(8)<< "-----" << setw(8)<<
            "-----" <<setw(8)<< "-----" << setw(8) << "-----" << endl;
    
    cout << setw(10) << left << "John" << setw(8) << john_1 << setw(8) << john_2
            << setw(8) <<john_3 <<setw(8)<< john_4 << endl;
    
    cout << setw(10) << left << "Mary" << setw(8)<< mary_1 << setw(8) <<
            mary_2 << setw(8)<< mary_3 <<setw(8)<< mary_4 << endl;
    
    cout << setw(10) << left << "Matthew" <<setw(8) << matthew_1 << setw(8) <<
            matthew_2 << setw(8) << matthew_3 << setw(8) << matthew_4 << endl;
    
    cout << endl;
    
    quiz_1_average = (john_1 + mary_1 + matthew_1)/3;
    quiz_2_average = (john_2 + mary_2 + matthew_2)/3;
    quiz_3_average = (john_3 + mary_3 + matthew_3)/3;
    quiz_4_average = (john_4 + mary_4 + matthew_4)/3; 
    
    cout << setw(10) << left << "Average" << setw(8) << quiz_1_average << setw(8)<<
            quiz_2_average << setw(8) << quiz_3_average << setw(8) <<
            quiz_4_average << endl;
    
    return 0;
}


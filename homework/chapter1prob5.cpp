#include <iostream>

using namespace std;

int main()
{
    int first_integer, second_integer, sum_of_integers, product_of_integers;
    cout << "Press enter to begin.\n";
    cout << "Enter an integer.\n";
    cin >> first_integer;
    cout << "Enter a second integer.\n";
    cin >> second_integer;
    
    sum_of_integers = first_integer + second_integer;
    product_of_integers = first_integer * second_integer;
    
    cout << "The sum of ";
    cout << first_integer;
    cout << " and ";
    cout << second_integer;
    cout << " is ";
    cout << sum_of_integers << endl;
    
    cout << "The product of ";
    cout << first_integer;
    cout << " and ";
    cout << second_integer;
    cout << " is ";
    cout << product_of_integers << endl;
    
    cout << "This is the end of the program.\n";
    
    
    return 0;
}

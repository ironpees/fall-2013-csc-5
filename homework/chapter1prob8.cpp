/* 
 * File:   main.cpp
 * Author: abel
 *
 * Created on September 2, 2013, 11:46 PM
 */

#include <iostream>

using namespace std;

int main()
{
    int quarters, dimes, nickles, cents;
    cout << "Press enter to begin.\n";
    cout << "How many quarters do you have?\n";
    cin >> quarters;
    cout << "How many dimes do you have?\n";
    cin >> dimes;
    cout << "How many nickles do you have?\n";
    cin >> nickles;
    
    cents = quarters + dimes + nickles;
    
    cout << "If you have ";
    cout << quarters;
    cout << " quarters,";
    cout << dimes;
    cout << " dimes,and ";
    cout << nickles;
    cout << " nickles." << endl;
    cout << "Then you have ";
    cout << cents;
    cout << " cents in total!" << endl;
    cout << "Good-Bye";

    return 0;
}


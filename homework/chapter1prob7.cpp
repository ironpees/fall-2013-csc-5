/* 
 * File:   main.cpp
 * Author: abel
 *CS block letter
 * Created on September 2, 2013, 10:40 PM
 */

#include <iostream>

using namespace std;

int main()
{
    cout << "************************\n";
    cout << "                        \n";
    cout << "    CCC       SSSS   !! \n";
    cout << "   C   C     S    S  !! \n";
    cout << "  C          S       !! \n";
    cout << "  C           SSSS   !! \n";
    cout << "  C               S  !! \n";
    cout << "   C   C     S    S     \n";
    cout << "    CCC       SSSS   00 \n";
    cout << "                        \n";
    cout << "************************\n";
    cout << "Computer Science is cool stuff!!\n";

    
    return 0;
}
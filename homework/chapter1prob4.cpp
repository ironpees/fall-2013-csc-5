/* 
 * File:   main.cpp
 * Author: abel
 *
 * Created on September 2, 2013, 10:08 PM
 */

#include <iostream>

using namespace std;

/*
 * 
 */


int main()
{
    //user input data
    int number_of_pods, peas_per_pod, total_peas;
    cout << "Press return after entering a number.\n";
    cout << "Enter an amount of pea pods.\n";
    cin >> number_of_pods;
    cout << "Enter the amount of peas in a pod.\n";
    cin >> peas_per_pod;
    
    total_peas = number_of_pods + peas_per_pod;
    
    //output message for calculations
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods \n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n";
    cout << "Good-bye\n";
    
    return 0;
}